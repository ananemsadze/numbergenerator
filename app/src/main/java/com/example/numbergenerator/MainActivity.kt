package com.example.numbergenerator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    private fun init(){
        val Generator = findViewById<Button>(R.id.Generator)
        val RandomNumberTV = findViewById<TextView>(R.id.RandomNumberTV)
        val YesNo = findViewById<TextView>(R.id.YesNo)
        Generator.setOnClickListener {
            val number:Int = randomNumber()
            d("generateNumber", "This is Random Number ${dividedbyFive(number)}")
            RandomNumberTV.text = number.toString()
            YesNo.text = dividedbyFive(number)



        }
    }
    private fun randomNumber() = (-100..100).random()
    fun dividedbyFive(randomNumber:Int):String {
        if (randomNumber%5==0 && randomNumber>0){
            return "yes"
        }else{
            return "no"
        }
    }

}

